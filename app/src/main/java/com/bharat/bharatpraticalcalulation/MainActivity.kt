package com.bharat.bharatpraticalcalulation

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.bharat.bharatpraticalcalulation.Controller.CalculationController
import java.util.*


class MainActivity : AppCompatActivity() {
    var controller: CalculationController? = null
    var textArea: EditText? = null
    var textCalculation: TextView? = null
    var text: String? = null
    var errorMessageText: TextView? = null
    var stackCalculation = Stack<String>()
    var calIndex = 0;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        textArea = findViewById(R.id.et_title)
        textCalculation = findViewById(R.id.tvCalculation)
        errorMessageText = findViewById(R.id.error_message)
//        errorMessageText.text  = "df"

    }
    fun onClickButton(view: View) {
        errorMessageText!!.visibility = View.INVISIBLE
        val buttonText = (view as Button).getText() as String
        text = textArea!!.text.toString()
        text += buttonText
        textArea!!.setText(text)
    }

    //TO clear input text & stack history
    fun clearInput(view: View) {
        textArea!!.setText("")
        stackCalculation.clear()
    }

    //Update Operation History Stack
    fun updateOperation(view: View){
        val textView = (view as TextView).getText() as String
        if (textView == "PREV"){
            if (calIndex == 0) return
            textCalculation?.text = stackCalculation[--calIndex]
        } else if (calIndex < stackCalculation.size - 1){
            textCalculation?.text = stackCalculation[++calIndex]
        }
    }

    fun evaluateExpression(view: View?) {
        controller = CalculationController()
        text = textArea!!.text.toString()
        val postfixExpression: String = controller!!.findPostfix(text)
        if (postfixExpression == "INVALID") {
            errorMessageText!!.visibility = View.VISIBLE
            textArea!!.setText("")
            return
        }
        val result: Int = controller!!.evaluatePostfix(postfixExpression, stackCalculation)
        textArea!!.setText("" + result)
        calIndex = 0
        textCalculation?.text = stackCalculation.firstElement()
    }
}