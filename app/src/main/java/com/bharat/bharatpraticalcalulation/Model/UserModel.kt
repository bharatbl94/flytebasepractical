package com.bharat.bharatpraticalcalulation.Model

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
data class UserModel (
    val userName: String? = null,
    val userPassword: String? = null
)