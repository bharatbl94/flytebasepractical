package com.bharat.bharatpraticalcalulation

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bharat.bharatpraticalcalulation.Model.UserModel
import com.bharat.bharatpraticalcalulation.databinding.ActivityLoginBinding
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.database.ktx.getValue
import com.google.firebase.ktx.Firebase

class LoginActivity : AppCompatActivity() {

    private lateinit var mBinding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        var valueUserData : UserModel? = null
        mBinding.btnLogin.setOnClickListener{
            if (valueUserData==null)
                Toast.makeText(this@LoginActivity, getString(R.string.wait), Toast.LENGTH_SHORT).show()
            if (mBinding.etUsername.text.toString() == valueUserData?.userName && mBinding.etPassword.text.toString() == valueUserData?.userPassword){
                val intent = Intent(this@LoginActivity, MainActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
            } else{
                Toast.makeText(this@LoginActivity, getString(R.string.wrong_input), Toast.LENGTH_SHORT).show()
            }
        }


        val database = Firebase.database
        val myRef = database.getReference("LoginData")
//        myRef.setValue("Hello, World!")
        // Read from the database
        myRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                valueUserData = dataSnapshot.getValue<UserModel>()
            }

            override fun onCancelled(error: DatabaseError) {
                Log.d("Error", error.toString())
            }
        })


    }
}